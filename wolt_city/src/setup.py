from setuptools import setup

setup(
    name='wolt_city',
    version='1.0.0',
    packages=[''],
    url='',
    license='',
    author='Matar Maoz',
    author_email='matar.maoz@gmail.com',
    description='This package is allowing performing actions in a Wolt city: activating and deactivating houses, '
                'viewing active houses and so on'
)
