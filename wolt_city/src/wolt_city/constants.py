# Django API connection

DEFAULT_HOST = 'localhost'
DEFAULT_PORT = 8000
URL_TEMPLATE = 'http://{}:{}/api/'

# Django tables
HOUSES_TABLE = 'houses'
STREETS_TABLE = 'streets'
WOLT_GUYS_TABLE = 'wolt_guys'
EXEC_INFOS_TABLE = 'exec_infos'

GET_TABLE_FORMAT = '{}/'
GET_ITEM_BY_ID_FORMAT = '{}/{}/'
SUB_FORMAT = 'sub/{}/'
UNSUB_FORMAT = 'unsub/{}/'
ONLINE_FORMAT = 'online/{}/'
PING_SUFFIX = 'ping/'

KEY_ID = 'id'
KEY_HOUSE = 'house'
KEY_STREET = 'street'
KEY_STREETS_IN = 'streets_in'
KEY_WOLT_GUY = 'wolt_guy'
KEY_ONLINE = 'online'
KEY_EXEC_INFO = 'exec_info'
KEY_NUM_OF_SUBSCRIBERS = 'num_subscribers'