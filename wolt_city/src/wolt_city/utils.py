import requests


def get_request(url, json=True):
    """
    Sends an HTTP GET request to the given URL and returns the answer (in Json or plain text).
    If an error code returned, an exception will be raised.

    :param url:     the url for the request
    :type url:      str
    :param json:    Answer will be json loaded if true, otherwise plain text. Default is Json
    :type json:     bool
    :return:        answer content from the server.
    """
    response = requests.get(url)
    response.raise_for_status()
    if json:
        return response.json()
    return response.content


def post_request(url, params):
    """
    Sends an HTTP POST request to the given URL with the given parameters.
    If an error code returned, an exception will be raised.

    :param url:     the url for the request
    :type url:      str
    :param params:  Parameters for the request body.
    :type params:   dict
    """
    response = requests.post(url, data=params)
    response.raise_for_status()
