from wolt_city import DEFAULT_HOST, DEFAULT_PORT, URL_TEMPLATE, \
    SUB_FORMAT, UNSUB_FORMAT, KEY_STREET, KEY_WOLT_GUY, \
    ONLINE_FORMAT, GET_TABLE_FORMAT, GET_ITEM_BY_ID_FORMAT, PING_SUFFIX, \
    get_request, post_request

from wolt_city.src.wolt_city import HOUSES_TABLE, KEY_EXEC_INFO, EXEC_INFOS_TABLE, KEY_NUM_OF_SUBSCRIBERS, \
    KEY_STREETS_IN, KEY_ID

from sorthestpathfinder.adapters.wolt_adapter import WoltAdapter

class CityClient:
    """
    A client for the Wolt Django server.
    The client lets you send tests_requests to the server for viewing items in the database, viewing the state
    of virtual houses, and subscribing WoltGuys to houses.
    """

    def __init__(self, host=DEFAULT_HOST, port=DEFAULT_PORT):
        """
        Constructor for the client, receives the host for the DB

        :param host:    The host of the DB API (default is localhost)
        :type host:     str
        :param port:    Port of the DB API (default is 8000)
        :type port:     int
        """
        self.api_url = URL_TEMPLATE.format(host, port)
        self.check_url()

    def check_url(self):
        """
        Checks if the server in the URL is up. If not, an exception will be thrown.
        """
        get_request(self.api_url + PING_SUFFIX, json=False)

    def get_table(self, table_name):
        """
        Pulls all the requested table's objects from the DB.

        :param table_name:      the URL suffix to request a specific table.
        :type table_name:       str
        :return:                list of dicts representing the table objects
        :rtype:                 list
        """
        return get_request(self.api_url + GET_TABLE_FORMAT.format(table_name))

    def get_item_by_id(self, table_name, item_id):
        """
        Pulls the item of the requested ID from the requested table in the DB.

        :param table_name:      the URL suffix to request a specific table.
        :type table_name:       str
        :param item_id:         ID of the item
        :type item_id:          int
        :return:                dict representing the item's fields
        :rtype:                 dict
        """
        return get_request(self.api_url + GET_ITEM_BY_ID_FORMAT.format(table_name, item_id))

    def is_house_online(self, house_id):
        """
        Returns True/False if the house is currently online.

        :param house_id:    the ID of the house
        :type house_id:     int
        :return:            boolean if online
        :rtype:             bool
        """
        return get_request(self.api_url + ONLINE_FORMAT.format(house_id))

    def subscribe_to_house(self, house_id, street_id, wolt_guy_id):
        """
        Unsubscribes the WoltGuy from a House, meaning the WoltGuy is planning on visiting the house.
        If this is the house's first subscriber, it will be turned online.
        Note that a house cannot have subscriptions from different streets.
        If the action failed an exception will be raised.

        :param house_id:        the ID of the house to visit
        :type house_id:         int
        :param street_id:       the ID of the street - note that the street must lead to the given house
        :type street_id:        int
        :param wolt_guy_id:     the id of the WoltGuy
        :type wolt_guy_id:      int
        """
        params = {KEY_STREET: street_id, KEY_WOLT_GUY: wolt_guy_id}
        post_request(self.api_url + SUB_FORMAT.format(house_id), params=params)

    def unsubscribe_from_house(self, house_id, wolt_guy_id):
        """
        Unsubscribes the WoltGuy from a House, meaning the WoltGuy is not planning on visiting the house.
        If the house has no more subscribers, it will be turned offline.
        If the action failed an exception will be raised.

        :param house_id:        the ID of the house
        :type house_id:         int
        :param wolt_guy_id:     the id of the WoltGuy
        :type wolt_guy_id:      int
        """
        params = {KEY_WOLT_GUY: wolt_guy_id}
        post_request(self.api_url + UNSUB_FORMAT.format(house_id), params=params)

    def retrieve_exec_info_of_house(self, house_id):
        house = self.get_item_by_id(HOUSES_TABLE, house_id)
        exec_id = house[KEY_EXEC_INFO]
        return self.get_item_by_id(EXEC_INFOS_TABLE, exec_id)

    def get_shortest_paths_to_house(self, house_id, houses_list, streets_list):
        for house in houses_list:
            if house[KEY_NUM_OF_SUBSCRIBERS]:
                exec_info = self.retrieve_exec_info_of_house(house[KEY_ID])
                active_street_on_house = exec_info[KEY_STREET]
                streets_to_delete = house[KEY_STREETS_IN].remove(active_street_on_house)
                streets_list = [street for street in streets_list if street not in streets_to_delete]
        return WoltAdapter(houses_list, streets_list).retrieve_shortest_path(house_id)