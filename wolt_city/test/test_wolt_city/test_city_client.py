import pytest
from requests import HTTPError

from wolt_city import CityClient

DEV_SERVER_PORT = 7999


@pytest.fixture(name='city_client', scope='session')
def fixture_city_client():
    """
    Fixture for the CityClient

    :return:    the CityClient
    :rtype:     CityClient
    """
    return CityClient(port=DEV_SERVER_PORT)


@pytest.mark.parametrize(
    ['house_id', 'street_id', 'wolt_guy_id', 'fail'],
    [(2, 1, 1, False), (2, 1, 2, False), (3, 2, 2, False), (4, 3, 2, False), (2, 1, 1, False),
     (2, 2, 2, True), (5, 1, 1, True), (4, 0, 2, True), (2, 1, 'str', True)])
def test_subscribe_to_house_works(city_client, house_id, street_id, wolt_guy_id, fail):
    """
    Tests the subscribe function works and fails when it should.

    :param city_client:     CityClient fixture
    :type city_client:      CityClient
    :param house_id:        ID of the house
    :type house_id:         int
    :param street_id:       ID of the street
    :type street_id:        int
    :param wolt_guy_id:     ID of the wolt guy
    :type wolt_guy_id:      int
    :param fail:            if this call should fail
    :type fail:             bool
    """
    if fail:
        with pytest.raises(HTTPError):
            city_client.subscribe_to_house(house_id, street_id, wolt_guy_id)
    else:
        city_client.subscribe_to_house(house_id, street_id, wolt_guy_id)


@pytest.mark.parametrize(
    ['house_id', 'wolt_guy_id', 'fail'],
    [(4, 2, False), (3, 2, False), (2, 1, False), (2, 2, False),
     (2, 2, True), (1, 1, True), (5, 1, True), (2, 'str', True)])
def test_unsubscribe_from_house_fails(city_client, house_id, wolt_guy_id, fail):
    """
    Tests the unsubscribe function and fails when it should.

    :param city_client:     CityClient fixture
    :type city_client:      CityClient
    :param house_id:        ID of the house
    :type house_id:         int
    :param wolt_guy_id:     ID of the wolt guy
    :type wolt_guy_id:      int
    :param fail:            if this call should fail
    :type fail:             bool
    """
    if fail:
        with pytest.raises(HTTPError):
            city_client.unsubscribe_from_house(house_id, wolt_guy_id)
    else:
        city_client.unsubscribe_from_house(house_id, wolt_guy_id)
