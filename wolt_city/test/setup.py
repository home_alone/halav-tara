from setuptools import setup

setup(
    name='test_wolt_city',
    version='1.0.0',
    packages=['pytest, wolt_city'],
    url='',
    license='',
    author='Matar Maoz',
    author_email='matar.maoz@gmail.com',
    description='Tests for WoltCity'
)
