from django.db.utils import IntegrityError
from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest, HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views.decorators import csrf, http
from django.core.exceptions import ValidationError

from views_utils import get_model, to_json_response, inject_model_parameters
from models import House
from models_utils import subscribe, unsubscribe
from virtual_houses import is_online


# Create your views here.


def index(request):
    """
    A view function for reaching the API's index

    :param request: the HTTP request
    :return:        an HTTP response
    """
    return HttpResponse("Welcome to the Wolt API!")


def ping(request):
    """
    A view function for pinging to the server.

    :param request: the HTTP request
    :return:        an HTTP response
    """
    return HttpResponse("Pong")


@http.require_GET
def get_all_objects(request, table_name):
    """
    A view function for requesting a table's objects.

    :param request:     the HTTP request
    :param table_name:  table name as written in the URL
    :type table_name:   str
    :return:            an HTTP response, a list of all table objects
    """
    model = get_model(table_name)
    objects = model.objects.all()
    return to_json_response(objects)


@http.require_GET
def get_object_by_pk(request, table_name, pk, field=None):
    """
    A view function for requesting an object from a table, using a primary key.
    If no such object exists, a 404 error would be raised.
    The function can also handle requests for a specific field in the object.

    :param request:     the HTTP request
    :param table_name:  table name as written in the URL
    :type table_name:   str
    :param pk:          the object's ID
    :type pk:           int
    :param field:       Optional, states the field of the object to focus on.
    :type field:        str
    :return:            an HTTP response, a json of the object
    """
    model = get_model(table_name)
    obj = get_object_or_404(model, pk=pk)
    if not field:
        return to_json_response(obj)
    if not hasattr(obj, field):
        return HttpResponseBadRequest('Field {} does not exist in table {}'.format(field, table_name))
    field_value = getattr(obj, field)
    return to_json_response(field_value)


@http.require_GET
def is_house_online(request, house_id):
    """
    A view function for asking whether a specific house is currently online.

    :param request:     the HTTP request
    :param house_id:    the ID of the House object
    :type house_id:     int
    :return:            an HTTP response, a JSON containing boolean True/False
    """
    get_object_or_404(House, pk=house_id)
    return to_json_response(is_online(int(house_id)))


@csrf.csrf_exempt
@http.require_POST
@inject_model_parameters(required=('street', 'wolt_guy'))
def subscribe_to_house(request, house_id, street=None, wolt_guy=None):
    """
    Function to handle POST request to subscribe a WoltGuy to a House, through a Street.
    The function adds an ExecInfo object, declaring the subscription, to the DB.
    The function also uses the inject_model_parameters to inject to it the model objects from the request.

    :param request:     the HTTP request
    :param house_id:    the ID of the House object
    :type house_id:     int
    :param street:      the Street object, given by the decorator
    :type street:       Street
    :param wolt_guy:    the WoltGuy object, given by the decorator
    :type wolt_guy:     WoltGuy
    :return:            an HTTP response, a JSON of the up-to-date ExecInfo
    """
    house = get_object_or_404(House, pk=house_id)
    try:
        exec_info = subscribe(house, street, wolt_guy)
    except (IntegrityError, ValidationError) as e:
        return HttpResponseBadRequest(e.message)
    return to_json_response(exec_info)


@csrf.csrf_exempt
@http.require_POST
@inject_model_parameters(required=('wolt_guy',))
def unsubscribe_from_house(request, house_id, wolt_guy=None):
    """
    Function to handle POST requests to unsubscribe a WoltGuy from a House through a certain Street.
    The ExecInfo object representing the subscription will be deleted.
    The function also uses the inject_model_parameters to inject to it the model objects from the request.

    :param request:     the HTTP request
    :param house_id:    the ID of the House object
    :type house_id:     int
    :param wolt_guy:    the WoltGuy object, given by the decorator
    :type wolt_guy:     WoltGuy
    :return:            an HTTP response, either the ExecInfo or a redirection to index
    """
    house = get_object_or_404(House, pk=house_id)
    try:
        exec_info = unsubscribe(house, wolt_guy)
    except ValidationError as exc:
        return HttpResponseBadRequest(exc.message)
    if exec_info:
        return to_json_response(exec_info)
    return HttpResponseRedirect(reverse(index))
