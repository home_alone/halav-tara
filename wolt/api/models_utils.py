from django.core.exceptions import ValidationError

from models import ExecInfo
from virtual_houses import is_online, activate_house, deactivate_house


def subscribe(house, street, wolt_guy):
    """
    Subscribes a WoltGuy to a House through the given Street. If this is the first subscriber, the house's
    ExecInfo will be created, otherwise it will just be modified.

    :param house:       the house
    :type house:        House
    :param street:      the street
    :type street:       Street
    :param wolt_guy:    the wolt guy
    :type wolt_guy:     WoltGuy
    :return:            the up-to-date ExecInfo
    :rtype:             ExecInfo
    """
    if street.house_dst.id is not house.id:
        raise ValidationError('Street must lead to the house')
    if not is_online(street.house_src.id):
        raise ValidationError(
            '{} cannot be subscribed to through {} because {} is not online'.format(house, street, street.house_src))

    if ExecInfo.objects.filter(house=house).exists():
        exec_info = house.exec_info
        if exec_info.street.id is not street.id:
            raise ValidationError('You may only subscribe to {} through {}'.format(house, house.exec_info.street))
    else:
        exec_info = ExecInfo(house=house, street=street)
        exec_info.save()
    exec_info.wolt_guys.add(wolt_guy)
    exec_info.save()

    if not is_online(house.id):
        activate_house(house.id)
    return exec_info


def unsubscribe(house, wolt_guy):
    """
    Unsubscribe a WoltGuy from a house, by removing him from the house's ExecInfo.
    If no WoltGuys are left subscribed, the ExecInfo is removed.

    :param house:       the house
    :type house:        House
    :param wolt_guy:    the wolt guy
    :type wolt_guy:     WoltGuy
    :return:            either the modified ExecInfo, or None if it has been deleted
    :rtype:             ExecInfo
    """
    try:
        exec_info = house.exec_info
    except Exception:
        raise ValidationError('{} has no subscribers'.format(house))
    if not exec_info.wolt_guys.filter(pk=wolt_guy.id).exists():
        raise ValidationError('{} was not subscribed to {}'.format(wolt_guy, house))
    exec_info.wolt_guys.remove(wolt_guy)
    if not exec_info.wolt_guys.exists():
        exec_info.delete()
        deactivate_house(house.id)
        return None
    exec_info.save()
    return exec_info
