from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet
from django.http import Http404, JsonResponse, HttpResponseBadRequest
from decorator import decorator

from models import House, Street, WoltGuy, ExecInfo

NAME_TO_MODEL = {
    model._meta.db_table: model for model in (House, Street, WoltGuy, ExecInfo)
}


def get_model(table_name):
    """
    Receives the table name and returns the model class.
    If the table name is invalid, a 404 error would be raised.

    :param table_name:  table name, as in the url
    :type table_name:   str
    :return:            the model class
    :rtype:             subclass of Model
    """
    if table_name not in NAME_TO_MODEL:
        table_name = table_name + 's'
        if table_name not in NAME_TO_MODEL:
            raise Http404('Table does not exist')
    return NAME_TO_MODEL[table_name]


def flatten_values(d):
    """
    Flattens the values in a dict, in case there's a list with only one item in it.

    :param d:   the dict
    :type d:    dict
    """
    for key in d:
        if isinstance(d[key], list) and len(d[key]) == 1:
            d[key] = d[key][0]


def to_json_response(response):
    """
    Receives an object or some value to return, and parses it into a Json response.

    :param response:    the response, could be any type of object
    :return:            a JsonResponse
    :rtype:             JsonResponse
    """
    if hasattr(response, 'fields_dict'):
        return JsonResponse(response.fields_dict())
    elif isinstance(response, QuerySet):
        return JsonResponse([obj.fields_dict() for obj in response.all()], safe=False)
    return JsonResponse(response, safe=False)


@decorator
def inject_model_parameters(func, required=None, *args, **kwargs):
    """
    This function is a decorator for POST view functions, that pulls the given keyword parameters from the request,
    converts them into model objects and injects them into the view function.
    In case the arguments are invalid or missing, a HttpResponseBadRequest would be returned.

    :param func:        the wrapped view function
    :param required:    the arguments required for the POST request, given as argument to the decorator
    :type required:     tuple
    :param args:        the function's args
    :param kwargs:      the function's kwargs
    :return:            the return value of the function after injecting to it the parameters
    """
    if not required:
        return func(*args, **kwargs)
    params = dict(args[0].POST)
    flatten_values(params)
    try:
        models_ids = [(model, params[model]) for model in required]
        total_args = tuple((arg for arg in args if arg)) + tuple(
            (get_model(model).objects.get(pk=pk) for model, pk in models_ids))
    except (KeyError, ObjectDoesNotExist):
        return HttpResponseBadRequest('One or more of parameters {} is either missing or invalid'.format(required))
    return func(*total_args, **kwargs)
