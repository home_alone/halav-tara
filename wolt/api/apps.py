from django.apps import AppConfig
from threading import Thread

from virtual_houses import monitor_online_houses


class WoltAPIConfig(AppConfig):
    name = 'api'
    verbose_name = 'Wolt Django API'

    def ready(self):
        """
        Initialization of the Django API, starts the thread monitoring the online houses
        """
        monitor_th = Thread(target=monitor_online_houses)
        monitor_th.start()
