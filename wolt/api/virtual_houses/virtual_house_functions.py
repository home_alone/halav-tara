import os
import signal
import subprocess
from datetime import datetime, timedelta

from constants import LOGS_DIRECTORY_PATH, FILENAME_FORMAT, HOUSE_TIMEOUT_SECONDS, LOG_DATE_FORMAT, \
    ACTIVATE_HOUSE_CMD_FORMAT


def is_house_online(house_id):
    """
    Receives the ID of a house, and checks if it is currently online by checking the last log.

    :param house_id:    ID of the House
    :type house_id:     int
    :return:            True if online, False if not
    :rtype:             bool
    """
    last_line = get_last_log_line(house_id)
    if not last_line:
        return False
    last_online = get_datetime_from_log_line(last_line)
    return datetime.now() - last_online < timedelta(seconds=HOUSE_TIMEOUT_SECONDS)


def get_house_pid(house_id):
    """
    Receives the ID of a house, and returns the process ID of the house.

    :param house_id:    ID of the House
    :type house_id:     int
    :return:            the process ID
    :rtype:             int
    """
    last_line = get_last_log_line(house_id)
    if not last_line:
        raise ValueError('Cannot get PID of house {} because it is not online'.format(house_id))
    return int(last_line.split()[2][4:])


def activate_house(house_id):
    """
    Activates the VirtualHouse with the given ID, by creating a new process for it. If the house is
    already online, an exception will be thrown.

    :param house_id:    ID of the House
    :type house_id:     int
    """
    if is_house_online(house_id):
        raise RuntimeError('House is already active')
    subprocess.Popen(ACTIVATE_HOUSE_CMD_FORMAT.format(house_id))


def deactivate_house(house_id):
    """
    Deactivates the VirtualHouse with the given ID, by killing its process.

    :param house_id:    ID of the House
    :type house_id:     int
    """
    if is_house_online(house_id):
        os.kill(get_house_pid(house_id), signal.SIGTERM)


def get_last_log_line(house_id):
    """
    Receives the ID of a house, and returns the last log line of the VirtualHouse if exists.
    If there are no lines or no file, None is returned.

    :param house_id:    ID of the House
    :type house_id:     int
    :return:            string of the last log line
    :rtype:             str
    """
    log_path = os.path.join(LOGS_DIRECTORY_PATH, FILENAME_FORMAT.format(house_id))
    if not os.path.exists(log_path):
        return None
    with open(log_path) as f:
        lines = f.readlines()
    if len(lines) == 0:
        return None
    return lines[-1]


def get_datetime_from_log_line(line):
    """
    Receives a string log line and returns the datetime of the log.

    :param line:    line from a log file
    :type line:     str
    :return:        date of the log
    :rtype:         datetime
    """
    date_str = line.split(',')[0]
    return datetime.strptime(date_str, LOG_DATE_FORMAT)


def clear_logs():
    """
    Clears the logs from the log folder.
    """
    all_logs_paths = os.listdir(LOGS_DIRECTORY_PATH)
    for log_path in all_logs_paths:
        os.remove(os.path.join(LOGS_DIRECTORY_PATH, log_path))
