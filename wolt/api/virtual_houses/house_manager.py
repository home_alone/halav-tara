from time import sleep
from threading import Lock

from virtual_house_functions import is_house_online

from ..models import House

MONITOR_UPDATE_INTERVAL = 2

online_houses = dict()
thread_lock = Lock()


def is_online(house_id):
    """
    Checks if the house is online and returns True/False

    :param house_id:        the house's ID
    :type house_id:         int
    :return:                True/False
    :rtype:                 bool
    """
    with thread_lock:
        return online_houses[house_id]


def monitor_online_houses():
    """
    A function for monitoring the houses currently online and updating the online_houses dict.
    Should be run with start in a sub process or thread.
    """
    global online_houses
    while True:
        with thread_lock:
            for house in House.objects.all():
                if house.is_home:
                    online_houses[house.id] = True
                else:
                    online_houses[house.id] = is_house_online(house.id)
        sleep(MONITOR_UPDATE_INTERVAL)
