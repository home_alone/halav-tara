import logging
import os
import sys
from random import randint
from time import sleep

from constants import LOGS_DIRECTORY_PATH, FILENAME_FORMAT, FAIL_ON_START_CHANCE, FAIL_CHANCE, \
    HOUSE_ONLINE_MESSAGE, HOUSE_LOG_INTERVAL, LOG_DATE_FORMAT, LOG_FORMAT, MAX_WAIT_BEFORE_START


def setup_logger():
    """
    Sets up the logger for the house to print logs with.

    :return:    a logger
    :rtype:     logging.RootLogger
    """
    formatter = logging.Formatter(fmt=LOG_FORMAT,
                                  datefmt=LOG_DATE_FORMAT)
    handler = logging.FileHandler(os.path.join(LOGS_DIRECTORY_PATH, FILENAME_FORMAT.format(house_id)),
                                  mode='a')
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    return logger


def fail(on_start=False):
    """
    Determines whether the house should fail. If this is on start, the chances of failure are higher.

    :param on_start:    Optional, determines whether the house is just starting. Defaults to False
    :type on_start      bool
    :return:            True if should fail, false otherwise
    :rtype:             bool
    """
    return randint(1, FAIL_ON_START_CHANCE) == 1 if on_start else randint(1, FAIL_CHANCE) == 1


def wait_until_start():
    """
    Waits a random amount of seconds before making the house online and sending logs.
    """
    sleep(randint(0, MAX_WAIT_BEFORE_START))


def start():
    """
    Starts the house, meaning now the house is online and will send logs updating its state.
    """
    wait_until_start()
    while not fail():
        log.info(HOUSE_ONLINE_MESSAGE)
        sleep(HOUSE_LOG_INTERVAL)


if __name__ == '__main__':
    house_id = sys.argv[1]
    log = setup_logger()
    if not fail(on_start=True):
        start()
