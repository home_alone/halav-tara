LOGS_DIRECTORY_PATH = 'C:\\workspace\\wolt\\wolt\\api\\virtual_houses\\logs'
HOUSE_SCRIPT_PATH = 'C:\\workspace\\wolt\\wolt\\api\\virtual_houses\\house_script.py'

ACTIVATE_HOUSE_CMD_FORMAT = 'python ' + HOUSE_SCRIPT_PATH + ' {}'
FILENAME_FORMAT = 'house_{}_logs.txt'

MAX_WAIT_BEFORE_START = 60
HOUSE_LOG_INTERVAL = 1
HOUSE_TIMEOUT_SECONDS = 5
HOUSE_ONLINE_MESSAGE = 'I am online'

LOG_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
LOG_FORMAT = '%(asctime)s, PID=%(process)d %(levelname)-8s %(message)s'

FAIL_ON_START_CHANCE = 50000
FAIL_CHANCE = 1000000

MONITOR_UPDATE_INTERVAL = 2
