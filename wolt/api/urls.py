from django.conf.urls import patterns, url

from views import index, get_all_objects, get_object_by_pk, subscribe_to_house, unsubscribe_from_house, \
    is_house_online, ping

urlpatterns = patterns('', url(r'^$', index, name='index'),
                       url(r'^ping/', ping, name='ping'),
                       url(r'^online/(?P<house_id>\d+)/$', is_house_online, name='online'),
                       url(r'^sub/(?P<house_id>\d+)/$', subscribe_to_house, name='subscribe'),
                       url(r'^unsub/(?P<house_id>\d+)/$', unsubscribe_from_house, name='unsubscribe'),
                       url(r'^(?P<table_name>\D+)/$', get_all_objects, name='table'),
                       url(r'^(?P<table_name>\D+)/(?P<pk>\d+)/$', get_object_by_pk, name='get_object'),
                       url(r'^(?P<table_name>\D+)/(?P<pk>\d+)/(?P<field>\D+)/$', get_object_by_pk,
                           name='get_object'),
                       )
