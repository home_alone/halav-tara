from django.contrib import admin
from models import House, Street, WoltGuy, ExecInfo

# Register your models here.

admin.site.register([House, Street, WoltGuy, ExecInfo])
