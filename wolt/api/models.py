from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.query import QuerySet


# Create your models here.


class House(models.Model):
    """
    A Model class for a house, connected to other houses through streets.
    """
    name = models.CharField(max_length=50)
    is_home = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def fields_dict(self):
        """
        Returns a dict representing the instance and its fields.
        :return:    dict mapping between field names and values
        :rtype:     dict
        """
        d = {'id': self.id, 'name': self.name, 'streets_in': get_id_list(self.streets_in),
             'streets_out': get_id_list(self.streets_out), 'num_subscribers': 0, 'home': self.is_home}
        if hasattr(self, 'exec_info'):
            d.update({'exec_info': self.exec_info.id, 'num_subscribers': self.exec_info.wolt_guys.count()})
        return d

    class Meta:
        db_table = "houses"


class Street(models.Model):
    """
    A Model class for a street, connecting two houses.
    """
    name = models.CharField(max_length=50)
    house_src = models.ForeignKey(House, related_name='streets_out')
    house_dst = models.ForeignKey(House, related_name='streets_in')

    def __str__(self):
        return self.name + ' St.'

    def fields_dict(self):
        """
        Returns a dict representing the instance and its fields.
        :return:    dict mapping between field names and values
        :rtype:     dict
        """
        d = {'id': self.id, 'name': self.name, 'house_src': self.house_src.id, 'house_dst': self.house_dst.id}
        if hasattr(self, 'exec_info'):
            d['exec_info'] = self.exec_info.id
        return d

    class Meta:
        db_table = "streets"


class WoltGuy(models.Model):
    """
    A Model class for a Wolt delivery guy
    """
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def fields_dict(self):
        """
        Returns a dict representing the instance and its fields.
        :return:    dict mapping between field names and values
        :rtype:     dict
        """
        return {'id': self.id, 'first_name': self.first_name, 'last_name': self.last_name,
                'exec_infos': get_id_list(self.exec_infos)}

    class Meta:
        db_table = "wolt_guys"


class ExecInfo(models.Model):
    """
    A model class for keeping track of user actions of planning routes through certain houses with certain wolt guys.
    """
    date = models.DateTimeField(auto_now=True)
    house = models.OneToOneField(House, related_name='exec_info')
    street = models.OneToOneField(Street, related_name='exec_info')  # the street's house_dst must be this house!
    wolt_guys = models.ManyToManyField(WoltGuy, related_name='exec_infos')

    def clean(self):
        """
        Overriding Model.clean(), This method checks if the street field matches the house field, and
        raises a ValidationError if not.
        """
        if self.street.house_dst.id is not self.house.id:
            raise ValidationError('Street must lead to the house')

    def __str__(self):
        return '{} are subscribed to {} through {}'.format(self.wolt_guys.all(), self.house, self.street)

    def fields_dict(self):
        """
        Returns a dict representing the instance and its fields.
        :return:    dict mapping between field names and values
        :rtype:     dict
        """
        return {'id': self.id, 'date': self.date, 'house': self.house.id, 'street': self.street.id,
                'wolt_guys': get_id_list(self.wolt_guys)}

    class Meta:
        db_table = 'exec_infos'


def get_id_list(query_set):
    """
    Receives a QuerySet and returns the IDs of all items in the set, in a list.

    :param query_set:   A QuerySet
    :type query_set:    QuerySet
    :return:            list of ids
    :rtype:             list of ints
    """
    return list(query_set.values_list('id', flat=True))


